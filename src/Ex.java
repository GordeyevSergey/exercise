import java.util.Arrays;
import java.util.Collections;

public class Ex {
    public static void main(String[] args) {
        int time = 5;
        Integer orders[] = inputOrders();
        outputOrders(orders);
        sort(orders);
        outputOrders(orders);
        System.out.println("Максимальная выручка за " + time + " единиц времени = " + searchProfitOrders(orders, time));


    }

    private static Integer[] inputOrders(){
        Integer orders[] = new Integer[10];
        for(int i = 0; i <  orders.length; i++) {
            orders[i] =  (int)(Math.random() * 100);
        }
        return orders;
    }

    private static void sort(Integer[] orders){
        Arrays.sort(orders, Collections.reverseOrder());
    }

    private static int searchProfitOrders(Integer[] orders, int time){
        int results = 0;
        for (int i=0; i<time; i++) results = results + orders[i];
        return results;
    }

    private static void outputOrders(Integer[] orders){
        for (Integer order : orders) {
            System.out.print(order + "  ");
        }
        System.out.println();
    }

}
